import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bmicalculator',
  templateUrl: './bmicalculator.component.html',
  styleUrls: ['./bmicalculator.component.css']
})
export class BmicalculatorComponent implements OnInit {

  title = 'BMI Calculator';
  public height:any;
  public weight:any;
  public p:any;
  public result:any;
  public msg:any;
  public img:any;



bmi(){
  this.p = (this.height/100)*(this.height/100);
  this.result =this.weight/this.p;
  if(this.result<16){
    this.msg="Severely Underweight";
  }else if(this.result>16.0 && this.result<18.4){
    this.msg="Underweight";
    
  }
  else if(this.result>18.5 && this.result<24.9){
    this.msg="Normal";

  } else if(this.result>30.0 && this.result<34.9){
    this.msg="Moderately Obese";
    this.router.navigate(['ov']);
  } 
  else if(this.result>35.0 && this.result<39.9){
    this.msg="Severely Obese";

  }else if(this.result> 40.0){
    this.msg="Morbidly Obese";

   }  
}
 
   constructor() {



    }

  ngOnInit(): void {
  }

}