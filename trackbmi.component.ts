import { Component, OnInit } from '@angular/core';
import { TrackService } from '../track.service';
//import { HttpClientModule } from '@angular/common/http';


@Component({
  selector: 'app-trackbmi',
  templateUrl: './trackbmi.component.html',
  styleUrls: ['./trackbmi.component.css']
})
export class TrackbmiComponent implements OnInit {

  track: any;
  constructor(private service:TrackService, ) { 
    this.track = {Id:'',Name:'',dob:'',bmi:''}
  }

  ngOnInit(): void {
  }

  
  TrackService(){
    this.service.TrackService(this.track).subscribe();
    
  }


}
