import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegService {

  userLoggedIn : boolean;

  constructor( private httpClient: HttpClient) {
    this.userLoggedIn = false;
   }

   getUserLoggedIn(): any {
     return this.userLoggedIn;
   }

   setUserLoggedIn(): void {
     this.userLoggedIn = true;
   }

   setUserLoggedOut(): void {
     this.userLoggedIn = false;
   }
   fetchDetails(){
     return this.httpClient.get('http://localhost:3000/fetch');
   }
   RegService(register: any){
     return this.httpClient.post('http://localhost:3000/register',register);
   }
   getUserByEmailAndPassword(loginForm: any){
    return this.httpClient.get('http://localhost:3000/login/'+loginForm.mail+"/"+loginForm.password).toPromise();
   }
}
